import os
import sys
import time
import logging
import shutil
from datetime import datetime
from weakref import WeakValueDictionary


class _RelativeTimeFilter(logging.Filter):
    """
    Record filter to have relative time in seconds for logging module
    """

    def filter(self, record):
        record.relativeSeconds = '{0:.3f}'.format(record.relativeCreated / 1000.0)
        return True


class BaseLogger(object):
    """
    BaseLogger class for all python apps to use
    """
    # store instances to return existing instances
    _instances = WeakValueDictionary()

    def __new__(cls, *args, **kwargs):
        # new factory to return existining instances if they exist
        name = kwargs.get('name')
        inst = cls._instances.get(name)
        if inst is not None:
            return inst
        else:
            # limit creation of loggers to 100 to prevent memory issue with rogue processes
            if len(cls._instances) >= 100:
                raise RuntimeError("Number of {} instances greater than maximum".format(cls.__name__))
            new_inst = super(BaseLogger, cls).__new__(cls)
            cls._instances[name] = new_inst
            return new_inst

    @classmethod
    def _get_logger(cls, *args, **kwargs):
        """
        Class method to create a new logger or get existing instance of a logger

        :param args: Can provide name as just a string
        :param name: logger name to reference with getlogger if not supplied in args
        :type name: str
        :param loglevel: Log level to set
        :type loglevel: int
        :param filename: Name of file or None to use default <datetime>_runlog.log
        :type filename: str
        :param logdir: Directory to save log file to
        :type logdir: str
        :param logformat: Logfile format default
        :type logformat: str
        :param dateformat: default format d/f/Y H:M:S
        :type dateformat: str
        :param file_delay: Delay writing to file until logs written default False
        :type file_delay: bool
        :return: New or Existing BaseLogger Instance
        :rtype: BaseLogger
        """
        # gets or creates a new logger from a name
        name = kwargs.get('name')
        try:
            if name is None and isinstance(args[0], str):
                name = args[0]
                kwargs['name'] = name
        except IndexError:
            pass

        if name is None:
            """
            returns the first class found
            This makes calls to get logger simpler with only one logger but not safe when using multiple loggers
            """
            for _name, inst in cls._instances.items():
                return inst
        elif cls._instances.get(name) is not None:
            # return exiting logger
            return cls._instances.get(name)
        else:
            # creat a new logger from passed params
            new_instance = cls(**kwargs)
            cls._instances[name] = new_instance
            return new_instance

    def __init__(self, *args, **kwargs):
        """
        Creates a logger class to handle logging to file, console and file & console
        """
        self.name = kwargs.get('name', 'root')
        filename = kwargs.get('filename')
        if filename is None:
            filename = '{name}_log'.format(name=self.name)

        logdir = kwargs.get('logdir')

        # set default logdir, use naem if not supplied
        if logdir is None:
            if 'logs' not in filename:
                postfix = 's' if 'log' in filename[-3:] else '_logs'
            else:
                postfix = ''
            logdir = os.path.expanduser('{name}{postfix}'.format(name=filename, postfix=postfix))

        loglevel = kwargs.get('loglevel', 'DEBUG')
        logformat = kwargs.get('logformat',
                               '(%(asctime)s%(relativeSeconds)s)<%(name)s>%(module)s::%(funcName)s : %(levelname)s: %(message)s')
        dateformat = kwargs.get('dateformat', '%y-%m-%d|%H:%M:%S|')
        self.__file_delay = kwargs.get('file_delay', True)
        disabled = kwargs.get('disabled', False)
        header = kwargs.get('header', False)

        # Create loggers
        self.file_console = logging.getLogger(self.name + "_root")
        self.console = logging.getLogger(self.name + "_console")
        self.file = logging.getLogger(self.name + "_file")

        log_level = logging.getLevelName(loglevel)
        relative_seconds_filter = _RelativeTimeFilter()

        # set file name
        self.__log_file_name = '{0}_{1}.txt'.format(filename,
                                                    datetime.now().strftime("%Y-%m-%d-%H_%M_%S"))
        self.__log_file_path = os.path.join(logdir, self.__log_file_name)

        if not os.path.exists(logdir):
            os.makedirs(logdir)
        self._log_formatter = logging.Formatter(fmt=logformat,
                                                datefmt=dateformat)
        self._log_formatter.converter = time.gmtime

        # Add file handling if not already added
        self._log_file_handler = logging.FileHandler(self.__log_file_path, delay=self.__file_delay)
        self._log_file_handler.addFilter(relative_seconds_filter)
        self._log_file_handler.setFormatter(self._log_formatter)
        self.file_console.addHandler(self._log_file_handler)
        self.file.addHandler(self._log_file_handler)
        # add console handlers
        log_console_handler = logging.StreamHandler(stream=sys.stdout)
        log_console_handler.addFilter(relative_seconds_filter)
        log_console_handler.setFormatter(self._log_formatter)
        self.file_console.addHandler(log_console_handler)
        self.console.addHandler(log_console_handler)

        # set levels (can use to change logging level for new logger instances but will affect all logging instances)
        self.file_console.setLevel(log_level)
        self.file.setLevel(log_level)
        self.console.setLevel(log_level)

        # set enabled state
        if disabled:
            self._disabled = True
        else:
            self._disabled = False

        # write logfile begin message if requested
        if header:
            with open(self.__log_file_path, 'a') as lg:
                lg.write("LOGFILE BEGIN: {0}\n".format(datetime.now().strftime("%a %b %d %H:%M:%S %Y")))

    def update_log_dir(self, new_dir):
        """
        Can change the path to the log file directly

        :param new_dir: Path to new directory
        :type new_dir: str
        :return: New log file path
        :rtype: str
        """
        # remove previous file handlers
        self.file.removeHandler(self._log_file_handler)
        self.file_console.removeHandler(self._log_file_handler)
        new_full_path = os.path.join(new_dir, self.__log_file_name)
        # create path if it doesn't exists
        if not os.path.exists(new_dir):
            os.makedirs(new_dir)

        # move previous logfile to new_dir
        # Make sure to handle if nothing written yet (i.e file doesn't exist)
        if os.path.exists(self.__log_file_path):
            shutil.move(self.__log_file_path, new_full_path)

        # update dir in class
        self.__log_file_path = new_full_path

        # create and add new handlers
        self._log_file_handler = logging.FileHandler(self.__log_file_path, delay=self.__file_delay)
        self._log_file_handler.setFormatter(self._log_formatter)
        self.file_console.addHandler(self._log_file_handler)
        self.file.addHandler(self._log_file_handler)

    @property
    def instances(self):
        return self._instances

    @property
    def log_file_path(self):
        # need to make sure if the path is changed from another instance we update it
        # first get the current file handler and update
        self._log_file_handler = self.file.handlers[0]
        # get the file path from new file logger
        self.__log_file_path = self._log_file_handler.baseFilename
        return self.__log_file_path

    @property
    def log_file_name(self):
        return self.__log_file_name

    @property
    def disabled(self):
        return self._disabled

    @disabled.setter
    def disabled(self, value):
        """
        Disable or enable logger

        :param value: True/False
        :type value: bool
        """
        if self._disabled != value:
            self.file.disabled = value
            self.console.disabled = value
            self.file_console = value
            self._disabled = value


def get_logger(*args, **kwargs):
    """
    Module method to get an existing logger or create a new one automatically

    :param args: Can provide name as just a string
    :param name: logger name to reference with getlogger if not supplied in args
    :type name: str
    :param loglevel: Log level to set
    :type loglevel: int
    :param filename: Name of file or None to use default <datetime>_runlog.log
    :type filename: str
    :param logdir: Directory to save log file to
    :type logdir: str
    :param logformat: Logfile format default
    :type logformat: str
    :param dateformat: default format d/f/Y H:M:S
    :type dateformat: str
    :param file_delay: Delay writing to file until logs written default False
    :type file_delay: bool
    :return: New or Existing BaseLogger Instance
    :rtype: BaseLogger
    """

    return BaseLogger._get_logger(*args, **kwargs)
